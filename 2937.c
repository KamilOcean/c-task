#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main()
{
  char c;
  char str[100];
  int i; // длина строки
  int count = 0;
  scanf("%s", &str);
  // Array To Inger
  int currentNumber = atoi(str);
  int nextNumber = currentNumber + 1;
  int prevNumber = currentNumber - 1;
  printf("The next number for the number %d is %d.\n", currentNumber, nextNumber);
  printf("The previous number for the number %d is %d.\n", currentNumber, prevNumber);
}
