#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int SumOfDigits(int);

int main()
{
  char str[100];
  int result = 0; // суммируем сюда
  int count = 0; // счётчик
  scanf("%s", &str);
  int len = strlen(str);
  while(count <= len - 1)
  {
    // printf("char: %c\n", str[count]);
    int ic = (int)str[count] - 48;
    // printf("int: %d\n", ic);
    result += ic;
    // printf("result: %d\n", result);
    // int d = (int)str[i];
    // printf("%d\n", d);
    count++;
  }
  int num = atoi(str);
  int good = SumOfDigits(num);
  printf("%d", result);
}

int SumOfDigits(int n)
{
  return n + 4;
}
